## Zawartość:
W repozytorium zamieszczam własne i nie tylko własne skrypty, które wykorzystuje na własnych komputerach.
Wiele z nich pochodzi z sieci i zostało dostosowane do moich potrzeb.
Jako zupełnemu amatorowi zdarza mi sie wykorzystać szkielet skryptu zmieniając całkowicie zastosowanie.
Nie wszystkie zawierają informacje o autorach oryginalnych skryptów, za to i za moją łamaną angielszczyzne z góry przepraszam.

## Contents:
In the repository, I put my own and not only my own scripts that I use on my own computers. Many of them are from the web and adapted to my needs.
As a complete amateur, I happen to use the script skeleton, completely changing the application.
Not all contain information about the authors of the original scripts, but I apologize in advance for that and for my broken English.
